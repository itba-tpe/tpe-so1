#ifndef __HASH
#define __HASH

#define _POSIX_SOURCE
#define _DEFAULT_SOURCE

#include <time.h>
#include <math.h>

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <semaphore.h>
#include <fcntl.h>

#include <sys/stat.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <signal.h>

#include <dirent.h>
#include <errno.h>

// All
#define FALSE 0
#define TRUE !(FALSE)
#define HASH_SIZE 200

//slave workload distribution
#define PERCENTAGE 0.4
#define ROOF 150
#define SLAVE_COUNT 15

// App and view
#define PATH_SIZE 120
#define SEM_AMOUNT 3
#define SHM_SIZE 0xC800
#define SEM_PERM 0644
#define SHMMEM_PERM 0666
#define KEY_PATH "application.c"
#define SHMSEM_NAME "/shmSem"
#define VIEWSEM_NAME "/viewSem"
#define STATUSSEM_NAME "/statusSem"

// For App
#define ARG_SIZE 5			//this is just to send the fd through argument to the slave process
#define PERCENT 0.4
#define WAIT_TIME 10
#define MAX_PID_SIZE 6

// For Slave
#define STD_IN 0
#define STD_OUT 1
#define STD_ERR 2

/*
Auxiliary function to handle errors
*/
void handleError (char *msg);

//receives a file path and returns md5 hash though md5sum in OUTPUT
int getHash (char* filePath, char* hashRet);

/*
reads from fd until it finds a '\0', and saves in line up to MAXLINE characters.
If there are more char than MAXLINE it burns characters until end of line.
returns >0 if it was successfull
returns 0 if it reached EOF
returns >0 if there was an error
*/
void readString (char*line);

/*
Auxiliary functions for the creation of the shared memory
*/
key_t createKey (const char * pathname, unsigned int projId);
int createShmMem (key_t key, size_t size, int shmflg);
char * attachShmMem (int shmId, const void *shmaddr, int shmflg);

/*
Auxiliary function to avoid the repetition of code while opening semaphores
*/
sem_t * semOpen(const char *name, int oflag, mode_t mode, unsigned int value);

/*
Conditioned semaphore post function. Only does sem_post(sem) if the
current value of sem is less than maxValue
*/
void condSemPost (sem_t * sem, int maxValue);

/*
Auxiliary function to close all the semaphores that were used 
*/
void closeSem (sem_t * semaphore[]);

/*
Free the semaphores and the shared memory in application
*/
void freeResources (sem_t *semaphores[], int shmId, char * shmPtr);

#endif
