
#include "hash.h"

//REMINDER
//0 stdin
//1 stdout
//2 stderr

//reads from STD_IN and calculates the md5hash
int main(void){

  //error values
  int status = 1;
  //"buffer" memories
  char line[PATH_SIZE];
  char hashResult[HASH_SIZE];


  do{

    readString(line);

    getHash(line,hashResult);

    status = write(STD_OUT,hashResult,strlen(hashResult)+1);

    if(status <= 0){
      handleError("slave.c: There was an error writing the hash.");
    }

  } while(status > 0);        //think of a more adequate condition, for now it reacts to errors

  return status;
}


void readString(char*line){
  int c=0; //string length

  c = read(STD_IN,line,PATH_SIZE);

  if(c == 0){
    perror("slave.c: slave can't read from pipe because it doesn't exist");
  } else if(c < 0){
    handleError("slave.c: error reading from the pipe");
  }

  return;
}

int getHash(char* filePath, char* hashRet){

  //values for the shell command
  const char *hashC = "md5sum -t ";
  int hashLen = strlen(hashC);
  //temporal place the output goes
  FILE* output;
  //loop iterators
  int i = 0;
  int c;

  char* shellInput = malloc(strlen(filePath) + hashLen + 1);
  snprintf(shellInput, strlen(filePath) + hashLen + 1, "%s%s", hashC , filePath);

  output = popen(shellInput,"r");
  free(shellInput);

  if(output == NULL){
    handleError("slave.c: there was an error trying to execute a hash.");
  }

  while ((c = fgetc(output)) != EOF && i < HASH_SIZE - 1 ) {
    hashRet[i++] = c;
  }
    hashRet[i] = '\0';

  if(pclose(output) < 0){
    handleError("slave.c: error closing internal file");
  }

  return i;
}
