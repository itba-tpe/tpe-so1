gcc -std=c99 -pedantic -Wall -pthread application.c auxFunctions.c -o application
gcc -std=c99 -pedantic -Wall -pthread slave.c auxFunctions.c -o slave
gcc -std=c99 -pedantic -Wall -pthread view.c auxFunctions.c -o view

if [[ $* == "--test" ]]; then
	
	rm -R scriptfiles > /dev/null 2> /dev/null
	mkdir scriptfiles
	
	echo "Creating 100 random files"
	for i in {1..100}
	do
		echo $i> scriptfiles/file$i
	done

	echo "Running our application:"
	./application scriptfiles/* > /dev/null
	echo "Sorting our program's output:"
	sort hashes > hashesSorted1
	cat hashesSorted1
	echo "Running md5sum:"
	md5sum scriptfiles/* > hashesMd5
	echo "Sorting md5sum's output:"
	sort hashesMd5 > hashesSorted2
	cat hashesSorted2

	DIFF=$(diff hashesSorted1 hashesSorted2)

	if [ "$DIFF" == "" ]
	then
		echo "Test successful. Outputs are the same"
	else
		echo $DIFF
	fi

	rm hashesMd5
	rm hashesSorted1
	rm hashesSorted2
	rm -R scriptfiles
fi