#include "hash.h"


//------------------------- AUXILIARY FUNCTIONS --------------------------------

void handleError (char * message) {
	perror(message);
	exit(EXIT_FAILURE);
} 

//------------------------------------------------------------------------------

key_t createKey (const char * pathname, unsigned int projId) {
	key_t key = ftok(pathname, projId);
	if (key == (key_t) -1) {
		handleError("ftok failed: ");
	}
	return key;
}

//------------------------------------------------------------------------------

int createShmMem (key_t key, size_t size, int shmflg) {
	int shmId = shmget(key, size, shmflg);
	if (shmId < 0) {
	     handleError("shmget failed: ");
	}
	return shmId;
}

//------------------------------------------------------------------------------

char * attachShmMem (int shmId, const void *shmaddr, int shmflg){
	char * ptr = (char *)shmat(shmId, (void*)0, 0);
	if (ptr == (char *) -1) {
		handleError("shmat failed: ");
	}
	return ptr;
}

//------------------------------------------------------------------------------

sem_t * semOpen (const char *name, int oflag, mode_t mode, unsigned int value) {
	sem_t * sem;
	if ((sem = sem_open(name, oflag, mode, value)) == SEM_FAILED){
		handleError("Creation of semaphore failed: ");
	}
	return sem;
}

//------------------------------------------------------------------------------

void condSemPost (sem_t * sem, int maxValue) {
	int curValue;
	if (sem_getvalue(sem, &curValue) == -1) {
		perror("Obtaining value of semaphore failed");
	}
	if (curValue < maxValue){
		sem_post(sem);	
	}
}

//------------------------------------------------------------------------------

void closeSem (sem_t *semaphores[]) {
	int i;

	for (i = 0; i < SEM_AMOUNT; i++){
		if (sem_close(semaphores[i]) == -1){
			perror("Closing of shm semaphore failed");
		}
	}
}

//------------------------------------------------------------------------------

void freeResources(sem_t *semaphores[], int shmId, char * shmPtr) {
	
	// Detach from shared memory
    if(shmdt(shmPtr) == -1) {
    	perror("Detachment of shared memory failed in application");
    }

    // Responsibility of the application to destroy the shared memory
    if(shmctl(shmId, IPC_RMID, NULL) == -1) {
    	perror("Destruction of shared memory failed");
    }
    
	// Closing the semaphores
	closeSem(semaphores);

	// Unlinking the semaphores
	if (sem_unlink(SHMSEM_NAME) == -1){
		perror("Unlinking of shm semaphore failed in application");
	}
	if (sem_unlink(VIEWSEM_NAME) == -1){
		perror("Unlinking of view semaphore failed in application");
	}
	if (sem_unlink(STATUSSEM_NAME) == -1){
		perror("Unlinking of status semaphore failed in application");
	}
}