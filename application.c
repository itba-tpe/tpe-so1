#include "hash.h"

int main (int argc, char *argv[]) {
	//Verifying input
	if (argc < 2) {
		handleError("Not enough arguments.");
	}

	// Prints the PID of application
	unsigned int myPid = getpid();
	char pidBuff[MAX_PID_SIZE + 1];		// + 1 to include the enter needed for scanf
	memset(pidBuff, 0, (MAX_PID_SIZE + 1)*sizeof(char));

	snprintf(pidBuff, MAX_PID_SIZE + 1, "%u\n", myPid);
	write(STD_OUT, pidBuff, MAX_PID_SIZE + 1);

	//Set initial amount of file (could be invalid files, but still is an entry)
	int files = argc - 1;
	int filesSize = argc - 1;

	//First file will be in argv[1]
	int next = 1;

	// SLAVES VARIABLE
	char * slave = "slave";
	int n = SLAVE_COUNT;
	pid_t id, pids[n];

	//This variable holds how many files are sent to each slave
	int initSize;

	if(filesSize <= SLAVE_COUNT){
		initSize = 1;
	} else if(filesSize <= ROOF * SLAVE_COUNT){
		initSize = (int) ((PERCENT * filesSize)/SLAVE_COUNT);
	}else {
		initSize = ROOF;
	}

	// Pipes Variables
	int fach[2];
	int chfa[2];
	int childPipes[n][2];		//0 = read, 1 = write, 2 = signalRead (obsolete)
	int i;						//index variable used in for loops

//-----------------------------------------------------------------------

	// SEMAPHOREs
	sem_t *shmSem;
	sem_t *viewSem;		// To wait for view to begin/finish
	sem_t *statusSem;

	// Creation of semaphores
	shmSem = semOpen(SHMSEM_NAME, O_CREAT, SEM_PERM, 1);
	viewSem = semOpen(VIEWSEM_NAME, O_CREAT, SEM_PERM, 0);
	statusSem = semOpen(STATUSSEM_NAME, O_CREAT, SEM_PERM, 0);

	// Store all the semaphores in an array
	sem_t *sems[3] = {shmSem, viewSem, statusSem};

//-----------------------------------------------------------------------

	// SHARED MEMORY VARIABLES
	int shmId;
	char *shmPtr;
	key_t shmKey;
	char *curWrite;	//current place where application show write

	// Creating the key
	shmKey = createKey(KEY_PATH, myPid);

	// Creating shared memory
	shmId = createShmMem(shmKey, SHM_SIZE, IPC_CREAT | SHMMEM_PERM);

	// Attaching it to the address space
	shmPtr = attachShmMem(shmId, (void*)0, 0);

	/* curWrites points to the beginning + 1 of the shared memory
	The first byte will be a flag so that view knows when all the files
	have been processed */
	curWrite = shmPtr + 1;
	/* 1 - NOT DONE
	   0 - DONE */
	*shmPtr = 1;

//-----------------------------------------------------------------------

	for (i = 0; i < n; i++) {

		//Making this specific parent/child pipe

		//Resetting the pipes
		if ( ( pipe(fach) < 0 ) || ( pipe(chfa) < 0 ) ) {
			//Something went wrong
			handleError("Failure to create pipe.");
		}

		id = fork();

		if (id == -1) {
			handleError ("Failure to create a child process.");
		}

		if (id == 0) {
			//Closing standard output
			close(1);

			//Redirecting the stdout to the child->father pipe
			dup(chfa[1]);
			close(chfa[0]);		//Since we won't read
			close(chfa[1]);		//Since we use stdout

			//Closing standard input
			close(0);

			//Redirecting the stdin to the father->child pipe
			dup(fach[0]);
			close(fach[0]);		//Since we use stdin
			close(fach[1]);		//Since we won't write

			//execl to the child process
			execl(slave, "", (char *) NULL);

			//We should never get here
			abort();

		} else {
			//We save our pipes
			childPipes[i][0] = chfa[0];			//We save the fd where we read what child i says
			close(chfa[1]);						//I don't want to write, child i should
			childPipes[i][1] = fach[1];			//We save the fd where we write to child j
			close(fach[0]);						//I don't want to read, child i should

			// Storing the slave's PID
			pids[i] = id;
		}
	}

	for (i = 0; i < initSize * SLAVE_COUNT && files > 0; i++) {
		write(childPipes[i%n][1], argv[next++], PATH_SIZE);
		files--;
	}

//-----------------------------------------------------------------------

	//Implement select
	fd_set set;				//This is the set we will lead with out warning pipes
	int nfds;
	char hash[HASH_SIZE];
	int childProgress[n];
	int appProgress = 0;

	memset(childProgress, 0, n*sizeof(int));	//Initializing array with 0

	while (appProgress < filesSize) {
		//Clear the set
		FD_ZERO(&set);

		//Add the fd to set
		nfds = 0;						//Arbitriary
		for (i = 0; i < n; ++i) {

			if (childPipes[i][0] > nfds) {
				nfds = childPipes[i][0];
			}

			FD_SET(childPipes[i][0], &set);
		}

		select(nfds+1, &set, NULL, NULL, NULL);

		for (i = 0; i < n; ++i) {
			if (FD_ISSET(childPipes[i][0], &set)) {
				//This means child i wrote to his pipe

				//Consume one Hash from the pipe
				int bytesRead = read(childPipes[i][0], hash, HASH_SIZE);

				//We add to the progress entry of this slave
				childProgress[i]++;

				sem_wait(shmSem);

				memcpy(curWrite, hash, bytesRead);
				bytesRead--;
				curWrite += bytesRead;

				sem_post(shmSem);

				/*
				If the semaphore is already in 1, dont want to post again.
				This will unblock view since there is something to read
				*/
				condSemPost(statusSem, 1);

				//We check with initSize, if its the same or more then hes flat out of data (R&L ref.)
				if (childProgress[i] >= initSize) {
					if (files > 0) {
						write(childPipes[i][1], argv[next++], PATH_SIZE);
						files--;
					}
				}

				//We now put hash into shared memory and notice view that we have new data

				//Before we end we mark we successfully
				appProgress++;
			}
		}
	}

	//FIXME testing state, killing all slaves like a madman
	for(i = 0 ; i < n ; i++ ){
		kill(pids[i], SIGKILL);
	}

//-----------------------------------------------------------------------

	// SAVING CREATED HASHES TO A FILE
	FILE *hashFile;
	char *hashStart = shmPtr + 1;

	if ((hashFile = fopen("hashes", "w")) == NULL){
		perror("Error opening the file");
	}
	fprintf(hashFile, "%s", hashStart);

	if (fclose(hashFile) == EOF) {
		perror("Error closing the file");
	}

//-----------------------------------------------------------------------

	/*
	WAITING WAIT_TIME SECONDS IF VISTA WAS NOT EXECUTED!
	Current time + WAIT_TIME = timeout
	*/
	struct timespec ts;
	int waitStatus;

	if (clock_gettime(CLOCK_REALTIME, &ts) == -1) {
		perror("clock_gettime failed: ");
	}
	ts.tv_sec += WAIT_TIME;

    while ((waitStatus = sem_timedwait(viewSem, &ts)) == -1 && errno == EINTR){
        continue;       /* Restart if interrupted by handler */
    }

    if (waitStatus == -1){
    	if (errno != ETIMEDOUT) {
    		// Error and not because of time out = something went wrong
    		perror("sem_timedwait failed: ");
  		}
    } else {
    	// viewSem was unblocked --> view exists so we wait till it finished before deleting everything
	    // No more files to process, switch the flag
		*shmPtr = 0;
		condSemPost(statusSem, 1);
    	sem_wait(viewSem);
    }

//-----------------------------------------------------------------------

    freeResources(sems, shmId, shmPtr);

	exit(EXIT_SUCCESS);
}
