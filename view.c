
#include "hash.h"

int main (int argc, char *argv[]) {
	unsigned int appPid;
	int shmId;        /* shared memory ID*/
	char *shmPtr;
	key_t shmKey;
	char *curRead;	// current position where view should read for shm memory
	sem_t *shmSem;
	sem_t *viewSem;
	sem_t *statusSem;

	// Read the PID of application
	if (argc > 1) {
		// Pid was recieved as an argument
		appPid = (unsigned int) atoi(argv[1]);
	} else {
		// Pid was sent as STDIN
		scanf("%u", &appPid);
	}

//---------------------------------- CREATION OF SEMAPHORES -----------------------------------------

	// Creation of semaphores
	shmSem = semOpen(SHMSEM_NAME, O_CREAT, SEM_PERM, 1);
	viewSem = semOpen(VIEWSEM_NAME, O_CREAT, SEM_PERM, 0);
	statusSem = semOpen(STATUSSEM_NAME, O_CREAT, SEM_PERM, 0);

	sem_t *sems[3] = {shmSem, viewSem, statusSem};

//--------------------------------- CREATION OF SHARED MEMORY ---------------------------------------
	// Creating key
	shmKey = createKey("application.c", appPid);

	// Creating shared memory
	shmId = createShmMem(shmKey, SHM_SIZE, IPC_CREAT | SHMMEM_PERM);	 // Do not want view to create it, it should already exist

	// Attaching it to the address space
	shmPtr = attachShmMem(shmId, (void*)0, 0);

	// In case view got to the flag before application
	curRead = shmPtr + 1;	// curRead points to the beginning + 1 of the shm mem
	// The first byte goes to the flag

//---------------------------------------------------------------------------------------------------

	/*Notifying application that view is running and that it should
	wait for view to finish executing */
	condSemPost(viewSem, 1);

	do {
		// Blocking until there is something to read
		sem_wait(statusSem);

		// Limiting the access to the shared memory to one process at a time
		sem_wait(shmSem);

		while (*curRead != 0) {
			printf("%c", *curRead++);
		}

		sem_post(shmSem);
	} while (*shmPtr != 0);

	//Indicating to application that view is done!
	condSemPost(viewSem, 1);

	closeSem(sems);

    // Detach from shared memory
    if (shmdt(shmPtr) == -1) {
    	perror("Detachment of shared memory failed in view");
    }

    exit(EXIT_SUCCESS);
}
